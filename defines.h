#ifndef DEFINES_H
#define DEFINES_H
#include <QString>

struct USER
{
    QString email;
    QString password;
};


enum REQUEST{
    CHECK_USER,
    LOGIN,
    REGISTER,
    UPLOAD,
    SERVER,
    AUTO_LOGIN
};


#define LINE 1
#define CIRCLE 2
#define RECT 3
#define ARROW 4
#define TEXT 5
#define PEN 6
#define RECT_WITH_NUM 7
#endif // DEFINES_H
