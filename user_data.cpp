#include "user_data.h"
#include "ui_user_data.h"

user_data::user_data(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::user_data)
{
    ui->setupUi(this);
    email = "";
    passwrod ="";
    phone = "";
}

user_data::~user_data()
{
    delete ui;
}

void user_data::on_pushButton_ok_clicked()
{
    email = ui->lineEdit_email->text();
    passwrod = ui->lineEdit_pass->text();
    phone = ui->lineEdit_phone->text();
    this->hide();
}
