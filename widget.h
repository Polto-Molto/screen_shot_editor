#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QThread>
#include <QPen>
#include <QBrush>
#include <QDir>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QClipboard>
#include "httpmanager.h"
#include "dialoglogin.h"
#include "top_screen.h"

enum ItemAnchor{
    NONE,
    FisrtPoint,
    SecondPoint,
    Pan
};

struct DrawItem
{
    int type;
    int id;
    QLine line;
    QString text;
    QPen pen;
    QBrush brush;
    Qt::BGMode text_bg_mode;
    QVector<QPoint> points;
    ItemAnchor selectPoint;
};

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
public:
    enum PUBLISH{
        PUBLISH_SAVE,
        PUBLISH_DIRECT,
        PUBLISH_INDIRECT,
        PUBLISH_COPY_IMAGE,
        PUBLISH_COPY_LINK
    };

public:
    explicit Widget(httpManager*, QWidget *parent = 0);
    ~Widget();
    QString image_file;


    QColor currentColor;
    int currentPenWidth;
    int currentShape;
    QVector<DrawItem*> items;
    QVector<DrawItem*> undo_draw_items;
    int rect_with_num;

    DrawItem *currentItem;
    bool draw_item_state;

    DrawItem *currentSelectedItem;
    bool move_item_state;
    PUBLISH publish_state;

signals:
    void showTools();
    void hideTools();

public slots:
    void move_screen(bool);

    void selectMode(bool);
    void renderToImage();
    void clear();
    void clearAll();
    void undo();
    void redo();
    void setShape(int);
    void setColor(QColor);
    void setPenWidth(int i);

    void drawArrow(DrawItem*);
    void drawRectWithNumber(DrawItem*);
    void setTextBGMode(int);

    void shotscreen();
    bool isPointInShape(DrawItem *item, QPoint point);
    bool isFirstPointIsAnchor(DrawItem *item, QPoint point);
    bool isSecondPointIsAnchor(DrawItem *item, QPoint point);
    bool isCenterPointIsAnchor(DrawItem *item, QPoint point);

    void loginAcceptedAction();
    void authorizationAction();
    void captureScreenAction();
    void capturePartAction();
    void helpAction();
    void quitAction();

    void shotScreenAndPublish();
    void imageToClipboard();
    void imageUrlToClipboard();
    void saveImageToFile();

protected:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseMoveEvent(QHoverEvent *);
    void paintEvent(QPaintEvent *);

private:
    Ui::Widget *ui;
    httpManager *http_mgr;
    QWidget* tools_widget;
    QPainter *_painter;
    bool select_mode;
    bool item_selected;
    QSize currentScreenSize;
    Qt::BGMode current_text_bg_mode;
    QClipboard *clip;

    QPoint mouse_pressed_pos;

public:
    bool authorization_status;
    QAction *authorization;
    QAction *capture_part;
    QAction *capture_screen;
    QAction *help;
    QAction *quit;

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    top_screen *topScreen;

};

#endif // WIDGET_H
