#include "widget.h"
#include <QApplication>
#include "tools.h"
#include "httpmanager.h"
#include <QSystemTrayIcon>
#include "dialoglogin.h"
#include "dropmenu.h"
#include <QWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if (!QSystemTrayIcon::isSystemTrayAvailable()) {
        return 1;
    }
    QApplication::setQuitOnLastWindowClosed(false);
    httpManager *http =  new httpManager;
    DialogLogin *dlg = new DialogLogin(http);
    dlg->hide();
    QObject::connect(http,SIGNAL(userStatus(REQUEST,bool)), dlg, SLOT(setUserStatus(REQUEST,bool)));

    Widget w(http);
    w.hide();
    QObject::connect((&w)->authorization,SIGNAL(triggered(bool)), dlg, SLOT(show()));

    dropMenu *drop_menu = new dropMenu;
    drop_menu->hide();

    tools tool;
    tool.setGeometry(100,0,0,0);
    tool.hide();    

    //QObject::connect(&w,SIGNAL(showTools()), &tool, SLOT(show()));

    QObject::connect(&w,SIGNAL(hideTools()), &tool, SLOT(hide()));
    QObject::connect(&w,SIGNAL(hideTools()), drop_menu, SLOT(hide()));
    QObject::connect(&w,SIGNAL(showTools()), &tool, SLOT(show()));

    QObject::connect(drop_menu,SIGNAL(save()),&w,SLOT(saveImageToFile()));
    QObject::connect(drop_menu,SIGNAL(copy()),&w,SLOT(imageToClipboard()));
    QObject::connect(drop_menu,SIGNAL(publish()),&w,SLOT(shotScreenAndPublish()));

    QObject::connect(http,SIGNAL(startUploadProgress()),&tool,SLOT(set_upload_progress_show()));
    QObject::connect(http,SIGNAL(endUploadProgress()),&tool,SLOT(set_upload_progress_hide()));
    QObject::connect(http,SIGNAL(setUploadProgress(int)),&tool,SLOT(set_upload_progress(int)));

    //QObject::connect(dlg,SIGNAL(loginDone()),&tool,SLOT(show()));
    QObject::connect(dlg,SIGNAL(loginDone()),&w,SLOT(loginAcceptedAction()));

    QObject::connect(dlg,SIGNAL(registerDone()),&tool,SLOT(show()));
    QObject::connect(dlg,SIGNAL(loginError()),&tool,SLOT(hide()));
    //QObject::connect(dlg,SIGNAL(loginDone()),&w,SLOT(showMaximized()));

    QObject::connect(&tool,SIGNAL(move_screen(bool)),&w,SLOT(move_screen(bool)));
    QObject::connect(&tool,SIGNAL(shape(int)),&w,SLOT(setShape(int)));
    QObject::connect(&tool,SIGNAL(undo()),&w,SLOT(undo()));
    QObject::connect(&tool,SIGNAL(redo()),&w,SLOT(redo()));
    QObject::connect(&tool,SIGNAL(colorChanged(QColor)),&w,SLOT(setColor(QColor)));
    QObject::connect(&tool,SIGNAL(penWidthChanged(int)),&w,SLOT(setPenWidth(int)));
    QObject::connect(&tool,SIGNAL(selectMode(bool)),&w,SLOT(selectMode(bool)));
    QObject::connect(&tool,SIGNAL(shotscreen()),&w,SLOT(shotscreen()));

    QObject::connect(&tool,SIGNAL(textBGstateChanged(int)),&w,SLOT(setTextBGMode(int)));
    QObject::connect(&tool,SIGNAL(showDropMenu(QRect)),drop_menu,SLOT(showDropMenu(QRect)));

    QObject::connect(&tool,SIGNAL(copyToClipboard()),&w,SLOT(imageToClipboard()));
    return a.exec();
}
