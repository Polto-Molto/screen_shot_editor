#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

public slots:
    void reset_frames_size();
    void setWindowFullScreenSize();
    QRect getCenterFrameGeometry();

protected :
    bool eventFilter(QObject *object, QEvent *event);

private:
    Ui::Widget *ui;
    QRect press_top_left_pos;
    QRect press_top_pos;
    QRect press_top_right_pos;

    QRect press_left_pos;
    QRect press_center_pos;
    QRect press_right_pos;

    QRect press_buttom_left_pos;
    QRect press_buttom_pos;
    QRect press_buttom_right_pos;

    QPoint press_pos;
    QPoint current_pos;
};

#endif // WIDGET_H
