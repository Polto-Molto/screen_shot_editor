#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QMouseEvent>

#include <QApplication>
#include <QDesktopWidget>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setStyleSheet("background-color:rgba(255,255,255,255);");
    setAttribute(Qt::WA_TranslucentBackground);
    ui->frame_center->setStyleSheet("background-color:rgba(255,255,255,255);");
    ui->frame_center->setAttribute(Qt::WA_TranslucentBackground);

    ui->frame_top->setStyleSheet("background-color:rgba(50,50,50,150);");
    ui->frame_top_left->setStyleSheet("background-color:rgba(50,50,50,150);");
    ui->frame_top_right->setStyleSheet("background-color:rgba(50,50,50,150);");
    ui->frame_buttom->setStyleSheet("background-color:rgba(50,50,50,150);");
    ui->frame_buttom_left->setStyleSheet("background-color:rgba(50,50,50,150);");
    ui->frame_buttom_right->setStyleSheet("background-color:rgba(50,50,50,150);");
    ui->frame_left->setStyleSheet("background-color:rgba(50,50,50,150);");
    ui->frame_right->setStyleSheet("background-color:rgba(50,50,50,150);");


    ui->frame_top_left->installEventFilter(this);
    ui->frame_top->installEventFilter(this);
    ui->frame_top_right->installEventFilter(this);
    ui->frame_left->installEventFilter(this);
    ui->frame_center->installEventFilter(this);
    ui->frame_right->installEventFilter(this);
    ui->frame_buttom_left->installEventFilter(this);
    ui->frame_buttom->installEventFilter(this);
    ui->frame_buttom_right->installEventFilter(this);
    setWindowFullScreenSize();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::setWindowFullScreenSize()
{
    this->setGeometry(QApplication::desktop()->screenGeometry());
    QSize size = QApplication::desktop()->size();
    size = size - QSize(50,50);

    ui->frame_center->setGeometry(25,25,size.width(),size.height());

    ui->frame_center->setFixedSize(size);
}

QRect Widget::getCenterFrameGeometry()
{
    qDebug() << ui->frame_center->geometry();
}

void Widget::reset_frames_size()
{
    ui->frame_right->setMinimumSize(QSize(0,0));
    ui->frame_right->setMaximumSize(QSize(1000,1000));

    ui->frame_left->setMinimumSize(QSize(0,0));
    ui->frame_left->setMaximumSize(QSize(1000,1000));

    ui->frame_top->setMinimumSize(QSize(0,0));
    ui->frame_top->setMaximumSize(QSize(1000,1000));

    ui->frame_top_left->setMinimumSize(QSize(0,0));
    ui->frame_top_left->setMaximumSize(QSize(1000,1000));

    ui->frame_top_right->setMinimumSize(QSize(0,0));
    ui->frame_top_right->setMaximumSize(QSize(1000,1000));

    ui->frame_buttom->setMinimumSize(QSize(0,0));
    ui->frame_buttom->setMaximumSize(QSize(1000,1000));

    ui->frame_buttom_left->setMinimumSize(QSize(0,0));
    ui->frame_buttom_left->setMaximumSize(QSize(1000,1000));

    ui->frame_buttom_right->setMinimumSize(QSize(0,0));
    ui->frame_buttom_right->setMaximumSize(QSize(1000,1000));

}


bool Widget::eventFilter(QObject *object, QEvent *event)
{

    if (event->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
        press_pos =  mouseEvent->pos();
        press_center_pos = ui->frame_center->geometry();

        press_top_pos = ui->frame_top->rect();
        press_top_left_pos = ui->frame_top_left->geometry();
        press_top_right_pos = ui->frame_top_right->geometry();

        press_buttom_pos = ui->frame_buttom->geometry();
        press_buttom_left_pos = ui->frame_buttom_left->geometry();
        press_buttom_right_pos = ui->frame_buttom_right->geometry();

        press_left_pos = ui->frame_left->geometry();
        press_right_pos = ui->frame_right->geometry();
    }

    if (event->type() == QEvent::MouseMove)
    {
        getCenterFrameGeometry();
        if (object == ui->frame_top ) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
            int diff = mouseEvent->pos().y() - press_pos.y();
            reset_frames_size();
            ui->frame_buttom_right->setFixedSize(press_buttom_right_pos.size());
            ui->frame_center->setFixedHeight(press_center_pos.height() - diff);
            ui->frame_center->setGeometry(
                        ui->frame_center->x(),
                        press_center_pos.y() + diff,
                        ui->frame_center->width() ,
                        ui->frame_center->height());
        }

        if (object == ui->frame_buttom) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
            int diff = mouseEvent->pos().y() - press_pos.y();
            reset_frames_size();
            ui->frame_top_left->setFixedSize(press_top_left_pos.size());
            ui->frame_center->setFixedHeight(ui->frame_center->height() + diff);
        }

        if (object == ui->frame_left) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
            int diff = mouseEvent->pos().x() - press_pos.x();
            reset_frames_size();
            ui->frame_buttom_right->setFixedSize(press_buttom_right_pos.size());
            ui->frame_center->setFixedWidth(press_center_pos.width() - diff);
            ui->frame_center->setGeometry(
                        press_center_pos.x()+diff,
                        press_center_pos.y(),
                        ui->frame_center->width(),
                        ui->frame_center->height() );

        }

        if (object == ui->frame_right) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
            int diff = mouseEvent->pos().x() - press_pos.x();
            reset_frames_size();
            ui->frame_top_left->setFixedSize(press_top_left_pos.size());
            ui->frame_center->setFixedWidth( ui->frame_center->width() + diff);
        }

        if (object == ui->frame_top_left) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
            QPoint diff = mouseEvent->pos() - press_pos;
            reset_frames_size();
            ui->frame_buttom_right->setFixedSize(press_buttom_right_pos.size());
            ui->frame_center->setFixedWidth(press_center_pos.width() - diff.x());
            ui->frame_center->setFixedHeight(press_center_pos.height() - diff.y());
            ui->frame_center->setGeometry(
                        press_center_pos.x()+diff.x(),
                        press_center_pos.y(),
                        press_center_pos.width(),
                        press_center_pos.height() );
        }

        if (object == ui->frame_buttom_right) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
            QPoint diff = mouseEvent->pos() - press_pos;
            reset_frames_size();
            ui->frame_top_left->setFixedSize(press_top_left_pos.size());
            ui->frame_center->setFixedWidth(ui->frame_center->width() + diff.x());
            ui->frame_center->setFixedHeight(ui->frame_center->height() + diff.y());
        }

        if (object == ui->frame_top_right) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
            QPoint diff = mouseEvent->pos() - press_pos;
            reset_frames_size();
            ui->frame_buttom_left->setFixedSize(press_buttom_left_pos.size());
            ui->frame_center->setFixedWidth(ui->frame_center->width() + diff.x());
            ui->frame_center->setFixedHeight(press_center_pos.height() - diff.y());

        }

        if (object == ui->frame_buttom_left) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
            QPoint diff = mouseEvent->pos() - press_pos;
            reset_frames_size();
            ui->frame_top_right->setFixedSize(press_top_right_pos.size());
            ui->frame_center->setFixedWidth(press_center_pos.width() - diff.x());
            ui->frame_center->setFixedHeight(ui->frame_center->height() + diff.y());
            ui->frame_center->setGeometry(
                        press_center_pos.x()+diff.x(),
                        press_center_pos.y(),
                        press_center_pos.width(),
                        press_center_pos.height() );
        }

        if (object == ui->frame_center) {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(event);
            QPoint diff = mouseEvent->pos() - press_pos;
            reset_frames_size();
            ui->frame_top_left->setFixedWidth(ui->frame_top_left->width() +diff.x());
            ui->frame_top_left->setFixedHeight(ui->frame_top_left->height()+diff.y() );
            ui->frame_center->setFixedSize(press_center_pos.size());
            ui->frame_center->setGeometry(
                        ui->frame_center->x()+diff.x(),
                        ui->frame_center->y()+diff.y(),
                        ui->frame_center->width(),
                        ui->frame_center->height() );
        }

    }
    return false;
}
