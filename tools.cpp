#include "tools.h"
#include "ui_tools.h"
#include "defines.h"
#include <QDebug>

tools::tools(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::tools)
{
    ui->setupUi(this);
    QButtonGroup *buttons =  new QButtonGroup;
    buttons->addButton(ui->pushButton_arrow);
    buttons->addButton(ui->pushButton_circle);
    buttons->addButton(ui->pushButton_line);
    buttons->addButton(ui->pushButton_rect);
    buttons->addButton(ui->pushButton_pen);
    buttons->addButton(ui->pushButton_text);
    buttons->addButton(ui->pushButton_rect_with_num);
    buttons->addButton(ui->pushButton_move);
    ui->progressBar_upload->hide();

    Qt::WindowFlags flags = windowFlags();
    setWindowFlags(flags  | Qt::X11BypassWindowManagerHint |Qt::FramelessWindowHint| Qt::WindowStaysOnTopHint);
    setStyleSheet("background-color:rgba(255,255,255,255);");
    setAttribute(Qt::WA_TranslucentBackground);
    QColor color = QColor::fromHsv(0,255,255).toRgb();
    Q_EMIT(colorChanged(color));
    QString r,g,b;
    QString color_str;
    color_str = r.setNum(color.red()) +","+g.setNum(color.green()) +","+b.setNum(color.blue());
    ui->label->setStyleSheet("background-color:rgba("+color_str+",255);");


 //   connect(ui->toolButton_share_menu,SIGNAL(clicked()),this,SLOT(shareMenuActions()));

    connect(ui->horizontalSlider_color,SIGNAL(valueChanged(int)),this,SLOT(setLabelColor(int)));
    connect(ui->horizontalSlider_pen_width,SIGNAL(valueChanged(int)),this,SIGNAL(penWidthChanged(int)));
    connect(ui->pushButton_move,SIGNAL(toggled(bool)),this,SIGNAL(selectMode(bool)));
    connect(ui->checkBox_textBG,SIGNAL(stateChanged(int)),this,SIGNAL(textBGstateChanged(int)));
}

tools::~tools()
{
    delete ui;
}

void tools::on_pushButton_drop_menu_clicked()
{
    Q_EMIT(showDropMenu(this->geometry()));
}

void tools::on_pushButton_direct_publish_clicked()
{
    Q_EMIT(copyToClipboard());
}

void tools::setLabelColor(int v)
{
    QColor color = QColor::fromHsv(v,255,255).toRgb();
    Q_EMIT(colorChanged(color));
    QString r,g,b;
    QString color_str;
    color_str = r.setNum(color.red()) +","+g.setNum(color.green()) +","+b.setNum(color.blue());
    ui->label->setStyleSheet("background-color:rgb("+color_str+");");
}


void tools::on_pushButton_circle_clicked()
{
    ui->pushButton_circle->setChecked(true);
    Q_EMIT(shape(CIRCLE));
}

void tools::on_pushButton_line_clicked()
{
    ui->pushButton_line->setChecked(true);
    Q_EMIT(shape(LINE));
}

void tools::on_pushButton_arrow_clicked()
{
    ui->pushButton_arrow->setChecked(true);
    Q_EMIT(shape(ARROW));
}

void tools::on_pushButton_pen_clicked()
{
    ui->pushButton_pen->setChecked(true);
    Q_EMIT(shape(PEN));
}

void tools::on_pushButton_rect_clicked()
{
    ui->pushButton_rect->setChecked(true);
    Q_EMIT(shape(RECT));
}

void tools::on_pushButton_text_clicked()
{
    ui->pushButton_text->setChecked(true);
    Q_EMIT(shape(TEXT));
}

void tools::on_pushButton_redo_clicked()
{
    Q_EMIT(redo());
}

void tools::on_pushButton_undo_clicked()
{
    Q_EMIT(undo());
}

void tools::on_pushButton_rect_with_num_clicked()
{
    ui->pushButton_rect_with_num->setChecked(true);
    Q_EMIT(shape(RECT_WITH_NUM));
}

void tools::on_pushButton_move_screen_clicked()
{
    Q_EMIT(move_screen(ui->pushButton_move_screen->isChecked()));

//    Q_EMIT(shotscreen());
}

void tools::set_upload_progress_show()
{
    ui->progressBar_upload->show();
    ui->progressBar_upload->setValue(0);

}

void tools::set_upload_progress_hide()
{
    ui->progressBar_upload->hide();
}

void tools::set_upload_progress(int i)
{
    ui->progressBar_upload->setValue(i);
}
