#include "widget.h"
#include "ui_widget.h"
#include <QPainter>
#include <QMouseEvent>
#include <QDebug>
#include <QtMath>
#include <textcell.h>
#include <QScreen>

#include "defines.h"
#include <QWindow>
#include <QTimer>
#include <QFileDialog>

Widget::Widget(httpManager *hm, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    http_mgr(hm)
{
    ui->setupUi(this);
    topScreen =  new top_screen;
    topScreen->hide();
    clip = QApplication::clipboard();
    setWindowFlags( Qt::FramelessWindowHint);

    publish_state = PUBLISH_INDIRECT;
    rect_with_num = 0;
    _painter = new QPainter;
    tools_widget = 0;
    currentItem = 0;
    currentSelectedItem = 0;
    item_selected = false;
    authorization_status = false;

    authorization = new QAction(tr("Authorization"), this);
    connect(authorization,SIGNAL(triggered(bool)), this, SLOT(authorizationAction()));

    capture_part = new QAction(tr("capture_part_screen"), this);
    connect(capture_part,SIGNAL(triggered(bool)), this, SLOT(capturePartAction()));

    capture_screen = new QAction(tr("capture_screen"), this);
    connect(capture_screen,SIGNAL(triggered(bool)), this, SLOT(captureScreenAction()));

    help = new QAction(tr("Help"), this);
    connect(help,SIGNAL(triggered(bool)), this, SLOT(helpAction()));

    quit = new QAction(tr("Quit"), this);
    connect(quit,SIGNAL(triggered(bool)), this, SLOT(quitAction()));

    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(authorization);
    trayIconMenu->addAction(help);
    trayIconMenu->addAction(quit);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setIcon(QIcon(":/icons/dark/icons/images-dark-bar/24.png"));
    trayIcon->show();

    setStyleSheet("background-color:rgba(255,255,255,255);");
    setAttribute(Qt::WA_TranslucentBackground);

    draw_item_state = true;
    move_item_state = false;

    currentColor = QColor::fromHsv(0,255,255).toRgb();
    currentPenWidth = 1;
    select_mode = false;
    currentScreenSize = QGuiApplication::primaryScreen()->size();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::loginAcceptedAction()
{
    trayIconMenu->insertAction(help,capture_screen);
    trayIconMenu->insertAction(capture_screen,capture_part);
}

void Widget::authorizationAction()
{

}

void Widget::move_screen(bool state)
{
    if(state)
    {
        qDebug() << "move screen";

        topScreen->showMaximized();
        topScreen->setFocus();
        topScreen->show();
        topScreen->raise();
    }
    else
    {
        this->showMaximized();
        this->raise();
    }
}

void Widget::captureScreenAction()
{
    Q_EMIT(showTools());
    topScreen->showMaximized();
    this->showMaximized();
    this->raise();
}

void Widget::capturePartAction()
{

}

void Widget:: helpAction()
{
    qDebug() << "help";
}

void Widget::quitAction()
{
    ((QApplication*) this->parent())->quit();
}

void Widget::selectMode(bool b)
{
    select_mode = b;
}

void Widget::setColor(QColor c)
{
    currentColor = c;
}

void Widget::setPenWidth(int i)
{
    currentPenWidth = i;
}

void Widget::setTextBGMode(int i )
{
    if(i == 0)
        current_text_bg_mode = Qt::TransparentMode;
    else
        current_text_bg_mode = Qt::OpaqueMode;
}

void Widget::redo()
{
    if(undo_draw_items.count() > 0)
    {
        items.push_back(undo_draw_items.last());
        undo_draw_items.pop_back();
        this->update();
    }
}

void Widget::undo()
{
    if(items.count() > 0)
    {
        undo_draw_items.push_back(items.last());
        items.pop_back();
        this->update();
    }
}

void Widget::setShape(int s)
{
    currentShape = s;
}

void Widget::clearAll()
{
    foreach (DrawItem* item, items)
        delete item;
    items.clear();

    foreach (DrawItem* item, undo_draw_items)
        delete item;
    undo_draw_items.clear();
    this->update();
    this->hide();
}

void Widget::clear()
{
    this->update();
}

void Widget::paintEvent(QPaintEvent *event)
{
    _painter->begin(this);
    _painter->setRenderHint(QPainter::Antialiasing,true);
    QLine l;
    QPen pen;
    QFont font;
    QBrush text_brush(Qt::white);

    foreach (DrawItem *currentDrawShape, items)
    {
        _painter->setPen(currentDrawShape->pen);
        _painter->setBrush(currentDrawShape->brush);

        l = currentDrawShape->line;

        switch (currentDrawShape->type) {
        case LINE:
            _painter->drawLine(l);
            break;

        case RECT:
            _painter->drawRect(QRect(l.p1(),l.p2()));
            break;

        case ARROW:
            drawArrow(currentDrawShape);
            break;

        case CIRCLE:
            _painter->drawEllipse(l.p1()
                                  ,l.p2().x() - l.p1().x()
                                  ,l.p2().y() - l.p1().y());
            break;

        case TEXT:
            drawArrow(currentDrawShape);
            font.setPixelSize(currentDrawShape->pen.width()*8);
            _painter->setBackground(text_brush);
            _painter->setBackgroundMode(currentDrawShape->text_bg_mode);
            _painter->setFont(font);
            _painter->drawText(l.p1(),currentDrawShape->text);
            break;

        case RECT_WITH_NUM:
            drawRectWithNumber( currentDrawShape);
            break;

        case PEN:
            _painter->drawPolyline(currentDrawShape->points);
            break;
        }
    }
    if(select_mode  && item_selected)
    {
        QPen pen;
        pen.setWidth(1);
        pen.setColor(Qt::black);
        QBrush brush;
        brush.setColor(Qt::black);
        brush.setStyle(Qt::SolidPattern);
        _painter->setPen(pen);
        _painter->setBrush(brush);

        l = currentSelectedItem->line;
        _painter->drawEllipse(l.p1(),6,6);
        _painter->drawEllipse(l.p2(),6,6);
        _painter->drawEllipse(l.center(),6,6);

        if(currentSelectedItem->selectPoint == FisrtPoint)
        {
            _painter->drawLine(l.p1().x(),0,l.p1().x(),currentScreenSize.height());
            _painter->drawLine(0,l.p1().y(),currentScreenSize.width(),l.p1().y());
        }
        else if(currentSelectedItem->selectPoint == SecondPoint)
        {
            _painter->drawLine(l.p2().x(),0,l.p2().x(),currentScreenSize.height());
            _painter->drawLine(0,l.p2().y(),currentScreenSize.width(),l.p2().y());
        }
    }
    if(!draw_item_state)
    {
        currentItem->pen.setColor(currentColor);
        currentItem->pen.setWidth(currentPenWidth);
        currentItem->brush.setStyle(Qt::NoBrush);
        _painter->setPen(currentItem->pen);
        _painter->setBrush(currentItem->brush);
        l = currentItem->line;
        switch (currentItem->type) {
        case LINE:
            _painter->drawLine(l);
            break;

        case RECT:
            _painter->drawRect(QRect(l.p1(),l.p2()));
            break;

        case ARROW:
            drawArrow(currentItem);
            break;

        case CIRCLE:
            _painter->drawEllipse(l.p1()
                                  ,l.p2().x() - l.p1().x()
                                  ,l.p2().y() - l.p1().y());
            break;

        case TEXT:
            drawArrow(currentItem);
            break;

        case RECT_WITH_NUM:
            drawRectWithNumber( currentItem);
            break;

        case PEN:
            _painter->drawPolyline(currentItem->points);
            break;
        }
    }
    _painter->end();
}

void Widget::drawRectWithNumber(DrawItem *item)
{
    _painter->drawRect(QRect(item->line.p1(),item->line.p2()));

    QBrush brush(item->pen.color(),Qt::SolidPattern);
    _painter->setBrush(brush);
    _painter->drawEllipse(item->line.p2(),12,12);

    QPen pen(Qt::white,1,Qt::SolidLine);
    QFont font;
    pen.setWidth(12);
    font.setPixelSize(12);
    _painter->setPen(pen);
    _painter->setFont(font);
    _painter->drawText(item->line.p2().x()-10,item->line.p2().y()-10,20,20,Qt::AlignCenter,item->text);
}

void Widget::drawArrow(DrawItem *item)
{
    QPainterPath path;
    QLine l = item->line;
    path.moveTo (l.p2().x(), l.p2().y());

    double slope = ((double)l.p2().y()-(double)l.p1().y())/((double)l.p2().x()-(double)l.p1().x());
    double degrees = 30.0 ;
    double degrees1 = 20.0 ;
    int arrow_out = 50;
    int arrow_in = 40;

    double d = qAtan(slope) - qDegreesToRadians(degrees);
    double d1 = qDegreesToRadians(90.0-degrees)-qAtan(slope);

    double d2 = qAtan(slope) - qDegreesToRadians(degrees1);
    double d21 = qDegreesToRadians(90.0-degrees1)-qAtan(slope);

    if(((double)l.p2().x()-(double)l.p1().x()) >= 0)
    {
        path.lineTo (l.p2().x() - arrow_out * qCos(d)
                     , l.p2().y() - arrow_out * qSin(d));

        path.lineTo (l.p2().x() - arrow_in * qCos(d2)
                     , l.p2().y() - arrow_in * qSin(d2));

        path.lineTo (l.p1().x(), l.p1().y());

        path.lineTo (l.p2().x() - arrow_in * qSin(d21)
                     , l.p2().y() - arrow_in * qCos(d21));

        path.lineTo (l.p2().x() - arrow_out * qSin(d1)
                     , l.p2().y() - arrow_out * qCos(d1));
    }
    else
    {
        path.lineTo (l.p2().x() + arrow_out * qCos(d)
                     , l.p2().y() + arrow_out * qSin(d));

        path.lineTo (l.p2().x() + arrow_in * qCos(d2)
                     , l.p2().y() + arrow_in * qSin(d2));

        path.lineTo (l.p1().x(), l.p1().y());

        path.lineTo (l.p2().x() + arrow_in * qSin(d21)
                     , l.p2().y() + arrow_in * qCos(d21));

        path.lineTo (l.p2().x() + arrow_out * qSin(d1)
                     , l.p2().y() + arrow_out * qCos(d1));
    }


    path.lineTo (l.p2().x(), l.p2().y());
    QPen pen;
    pen.setWidth(1);
    pen.setColor(item->pen.color());
    QBrush brush;
    brush.setColor(item->pen.color());
    brush.setStyle(Qt::SolidPattern);
    _painter->setPen(pen);
    _painter->fillPath(path,brush);
    _painter->drawPath(path);
}

void  Widget::mousePressEvent(QMouseEvent *event)
{
    mouse_pressed_pos = event->pos();
    if(select_mode)
    {
        if(item_selected)
        {
            bool state1 = false;
            bool state = false;
            state1 = isPointInShape(currentSelectedItem, mouse_pressed_pos);
            state = isFirstPointIsAnchor(currentSelectedItem,mouse_pressed_pos);
            if(state)
                currentSelectedItem->selectPoint = FisrtPoint;
            else
            {
                state = isSecondPointIsAnchor(currentSelectedItem,mouse_pressed_pos);
                if(state)
                    currentSelectedItem->selectPoint = SecondPoint;
                else
                {
                    state = isCenterPointIsAnchor(currentSelectedItem,mouse_pressed_pos);
                    if(state)
                        currentSelectedItem->selectPoint = Pan;
                }
            }
            if(state || state1)
                item_selected = true;
            else
            {
                item_selected = false;
                foreach (DrawItem *item, items)
                {
                    item_selected = isPointInShape(item, mouse_pressed_pos);
                    if(item_selected)
                    {
                        currentSelectedItem = item;
                        break;
                    }
                }
            }
        }
        else
        {
            item_selected = false;
            foreach (DrawItem *item, items)
            {
                item_selected = isPointInShape(item, mouse_pressed_pos);
                if(item_selected)
                {
                    currentSelectedItem = item;
                    break;
                }
            }
        }
    }
    else if(draw_item_state)
    {
        currentItem = new DrawItem;
        currentItem->type = currentShape;
        currentItem->line.setP1( mouse_pressed_pos);
        currentItem->line.setP2( mouse_pressed_pos);
        currentItem->text_bg_mode = current_text_bg_mode;
        draw_item_state = false;
        if(currentItem->type == RECT_WITH_NUM)
        {
            rect_with_num++;
            currentItem->text.setNum(rect_with_num);
        }
        currentItem->points.push_back(mouse_pressed_pos);
    }
    this->update();
}


void  Widget::mouseMoveEvent(QMouseEvent *event)
{
    if(select_mode)
    {
        if(item_selected)
        {
            QApplication::setOverrideCursor(Qt::SplitHCursor);
            if(currentSelectedItem->selectPoint == FisrtPoint)
                currentSelectedItem->line.setP1( event->pos());
            else if(currentSelectedItem->selectPoint == SecondPoint)
                currentSelectedItem->line.setP2( event->pos());
            else if(currentSelectedItem->selectPoint == Pan)
            {
                QPoint diff = event->pos() - currentSelectedItem->line.center();
                currentSelectedItem->line.translate(diff);
            }
        }

    }
    else if(!draw_item_state)
    {
        currentItem->line.setP2( event->pos());
        if(currentItem->type == PEN)
            currentItem->points.push_back(event->pos());
    }
    this->update();
}

void  Widget::mouseMoveEvent(QHoverEvent *event)
{
    qDebug() << event->pos();
}


void  Widget::mouseReleaseEvent(QMouseEvent *event)
{
    if(select_mode && item_selected )
    {
        QApplication::setOverrideCursor(Qt::ArrowCursor);
    }
    else if(!draw_item_state)
    {
        currentItem->line.setP2( event->pos());
        currentItem->id = items.count();
        switch (currentItem->type) {
        case TEXT:
        {
            TextCell *t = new TextCell(this);
            t->setGeometry(currentItem->line.p1().x() , currentItem->line.p1().y(), 100 ,100);
            t->raise();
            if(t->exec())
                currentItem->text = t->currentText();
            items.push_back(currentItem);
        }
            break;

        default:
            if(currentItem->line.p1() != currentItem->line.p2())
                items.push_back(currentItem);
            else
                rect_with_num --;
            break;
        }
        draw_item_state = true;
    }
    this->update();
}

void Widget::shotscreen()
{
    Q_EMIT(hideTools());
    QTimer::singleShot( 1000, this,SLOT(renderToImage()));
}

void Widget::renderToImage()
{
    QScreen *screen = QGuiApplication::primaryScreen();

    QPixmap pixmap = screen->grabWindow(0);

    QDir dir;
    QString file_dlg;

    switch(publish_state)
    {
    case PUBLISH_SAVE:
        file_dlg = QFileDialog::getSaveFileName(this,tr("Save Image"), "/home", tr("Image Files (*.png *.jpg *.bmp)"));
        pixmap.save(file_dlg);
        break;

    case PUBLISH_COPY_IMAGE:
        clip->setPixmap(pixmap,QClipboard::Clipboard);
        break;

    case PUBLISH_COPY_LINK:
        clip->setPixmap(pixmap,QClipboard::Clipboard);
        break;

    case PUBLISH_DIRECT:
        pixmap.save(dir.homePath()+"/pixmap.png");
        http_mgr->upload();
        break;

    case PUBLISH_INDIRECT:
        pixmap.save(dir.homePath()+"/pixmap.png");
        break;
    }
    clearAll();
    //Q_EMIT(showTools());
}

bool Widget::isPointInShape(DrawItem *item, QPoint point)
{
    bool state = false;
    QLine l = item->line;
    QMatrix mat;
    QPolygon p = mat.mapToPolygon(QRect(l.p1(),l.p2()));
    if(p.containsPoint(point,Qt::WindingFill))
        state = true;
    return state;
}

bool Widget::isFirstPointIsAnchor(DrawItem *item, QPoint point)
{
    bool state = false;
    if(item_selected)
    {
        QLine l = item->line;
        QPolygon p;
        QPoint p1,p2,p3,p4;
        p1 = QPoint(l.p1().x()-4 ,l.p1().y()-4);
        p2 = QPoint(l.p1().x()+4 ,l.p1().y()-4);
        p3 = QPoint(l.p1().x()+4 ,l.p1().y()+4);
        p4 = QPoint(l.p1().x()-4 ,l.p1().y()+4);

        p << p1<<p2<<p3<<p4<<p1;
        if(p.containsPoint(point,Qt::OddEvenFill))
            state = true;
    }
    return state;
}

bool Widget::isSecondPointIsAnchor(DrawItem *item, QPoint point)
{
    bool state = false;
    if(item_selected)
    {
        QLine l = item->line;
        QPolygon p;
        QPoint p1,p2,p3,p4;
        p1 = QPoint(l.p2().x()-4 ,l.p2().y()-4);
        p2 = QPoint(l.p2().x()+4 ,l.p2().y()-4);
        p3 = QPoint(l.p2().x()+4 ,l.p2().y()+4);
        p4 = QPoint(l.p2().x()-4 ,l.p2().y()+4);

        p << p1<<p2<<p3<<p4<<p1;
        if(p.containsPoint(point,Qt::OddEvenFill))
            state = true;

    }
    return state;
}

bool Widget::isCenterPointIsAnchor(DrawItem *item, QPoint point)
{
    bool state = false;
    if(item_selected)
    {
        QLine l = item->line;
        QPolygon p;
        QPoint p1,p2,p3,p4;
        p1 = QPoint(l.center().x()-4 ,l.center().y()-4);
        p2 = QPoint(l.center().x()+4 ,l.center().y()-4);
        p3 = QPoint(l.center().x()+4 ,l.center().y()+4);
        p4 = QPoint(l.center().x()-4 ,l.center().y()+4);

        p << p1<<p2<<p3<<p4<<p1;
        if(p.containsPoint(point,Qt::OddEvenFill))
            state = true;
    }
    return state;
}

void Widget::imageToClipboard()
{
    publish_state = PUBLISH_COPY_IMAGE;
    shotscreen();
}

void Widget::imageUrlToClipboard()
{
    publish_state = PUBLISH_COPY_LINK;
    shotscreen();
}

void Widget::saveImageToFile()
{
    publish_state = PUBLISH_SAVE;
    shotscreen();
}

void Widget::shotScreenAndPublish()
{
    publish_state = PUBLISH_DIRECT;
    shotscreen();
}
