#ifndef HTTPMANAGER_H
#define HTTPMANAGER_H

#include <QObject>
#include <QtNetwork>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QDesktopServices>
#include <QProgressBar>
#include "defines.h"
#include <QClipboard>
#include "datasettings.h"

class httpManager : public QObject
{
    Q_OBJECT
public:
    explicit httpManager(QObject *parent = 0);

public slots:
    bool checkUserFound(QString);

    bool registeration(QString ,QString ,QString);
    bool login(QString,QString);
    bool autoLogin();
    void upload();

    void replyFinished(QNetworkReply *);
    void uploadDone();
    void uploadProgress(qint64, qint64) ;

signals:
    void userStatus(REQUEST,bool);
    void startUploadProgress();
    void endUploadProgress();
    void setUploadProgress(int);
public slots:
    USER currentUser();

private:
    datasettings setting;
    QNetworkAccessManager *networkManager ;
    QProgressBar *progress;
    REQUEST current_request;
    USER current_user;
    bool isUserFound;
    bool isUserLogin;
    bool isUserRegister;
    QClipboard *clip;
};

#endif // HTTPMANAGER_H
