#ifndef DATASETTINGS_H
#define DATASETTINGS_H

#include <QObject>
#include <QSettings>

class datasettings : public QObject
{
    Q_OBJECT
public:
    explicit datasettings(QObject *parent = 0);

public slots:
    void setSettingFile(QString);
    void saveRegisterData(QString,QString);
    QString getUserName();
    QString getPassword();

signals:

public slots:
private:
    QSettings *setting;
};

#endif // DATASETTINGS_H
