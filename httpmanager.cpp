#include "httpmanager.h"
#include <QApplication>
#include <QMessageBox>
#include <QDebug>
#include <QCryptographicHash>

httpManager::httpManager(QObject *parent) : QObject(parent)
{
    clip = QApplication::clipboard();
    networkManager = new QNetworkAccessManager(this);
    progress = 0;
    isUserFound = false;
    isUserLogin = false;
    isUserRegister = false;
    setting.setSettingFile("reg.ini");
    autoLogin();
}

USER httpManager::currentUser()
{
    return current_user;
}

bool httpManager::checkUserFound(QString email_str)
{
    current_request = CHECK_USER;
    QUrl url("http://vasilevka.ru/api/users/check/");
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    // 1) email=sem@mail.ru
    // 2) password=hashed_password_here
    // 3) phone=79046859069
    QUrlQuery postData;
    postData.addQueryItem("email", email_str);
    postData.addQueryItem("action", "searchuser");
    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished(QNetworkReply *)));
    networkManager->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    return false;

}

bool httpManager::registeration(QString email_str,QString password_str,QString phone_str)
{
    current_request = REGISTER;
    current_user.email = email_str;
    current_user.password = password_str;

    QUrl url("http://vasilevka.ru/api/users/add/");
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    // 1) email=sem@mail.ru
    // 2) password=hashed_password_here
    // 3) phone=79046859069
    QUrlQuery postData;
    postData.addQueryItem("email", current_user.email);
    postData.addQueryItem("password", current_user.password);
    postData.addQueryItem("phone", phone_str);
    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished(QNetworkReply *)));
    networkManager->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    return true;
}

bool httpManager::login(QString email_str,QString password_str)
{
    current_request = LOGIN;
    current_user.email = email_str;
    current_user.password = password_str;

    QUrl url("http://vasilevka.ru/api/users/check/");
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    // 1) email=sem@mail.ru
    // 2) password=hashed_password_here
    QUrlQuery postData;
    postData.addQueryItem("email", current_user.email);
    postData.addQueryItem("password", current_user.password);
    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished(QNetworkReply *)));
    networkManager->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    return true;
}

bool httpManager::autoLogin()
{
    current_request = AUTO_LOGIN;
    current_user.email = setting.getUserName();
    current_user.password = setting.getPassword();

    QUrl url("http://vasilevka.ru/api/users/check/");
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    // 1) email=sem@mail.ru
    // 2) password=hashed_password_here
    QUrlQuery postData;
    postData.addQueryItem("email", current_user.email);
    postData.addQueryItem("password", current_user.password);
    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished(QNetworkReply *)));
    networkManager->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    return true;
}


void httpManager::upload()
{


    //1) email = sem@mail.ru
    //2) password = password
    //3) url = brR5Zd7fJ6EBdA - screenshot urs.. on which it will be available. for example http://joxi.ru/brR5Zd7fJ6EBdA
    //4) screenshot = $_FILE

    current_request = UPLOAD;
    QDir dir;

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart email;
    email.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"email\""));
    email.setBody(current_user.email.toUtf8());

    QHttpPart password;
    password.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"password\""));
    password.setBody(current_user.password.toUtf8());

    QHttpPart URL;
    URL.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"url\""));
    URL.setBody("url");


    QFile *file = new QFile(dir.homePath()+"/pixmap.png");

    QHttpPart screenshot;
    screenshot.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/png"));
    screenshot.setHeader(QNetworkRequest::ContentDispositionHeader
                         , QVariant("form-data; name=\"screenshot\"; filename=\"pixmap.png\""));

    file->open(QIODevice::ReadOnly);
    screenshot.setBodyDevice(file);
    file->setParent(multiPart); // we cannot delete the file now, so delete it with the multiPart

    qDebug() << current_user.email << current_user.password << file->fileName();
    multiPart->append(email);
    multiPart->append(password);
    multiPart->append(URL);
    multiPart->append(screenshot);

   // QUrl url("http://requestb.in/1d6weim1");
    QUrl url("http://vasilevka.ru/api/screenshots/upload/");

    QNetworkRequest request(url);

    QNetworkReply *reply = networkManager->post(request, multiPart);
    multiPart->setParent(reply); // delete the multiPart with the reply

    Q_EMIT(startUploadProgress());

    connect(reply, SIGNAL(uploadProgress(qint64, qint64)),
            this,SLOT(uploadProgress(qint64, qint64)));

    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished(QNetworkReply *)));
    connect(reply, SIGNAL(finished()), this, SLOT  (uploadDone()));
}

void httpManager::replyFinished(QNetworkReply *reply)
{
    QByteArray bts = reply->readAll();
    QString str(bts);
    str.trimmed();
    QString check_str;
    if(str.size() > 0 )
        qDebug() << str;
    if(str.size() > 0 && str.indexOf("result") > -1)
    {
        switch (current_request) {
        case CHECK_USER:
            check_str = "User found";
            if(str.indexOf(check_str) > -1)
            {
                isUserFound = true;
                Q_EMIT(userStatus(current_request, true));
            }
            else
            {
                isUserFound = false;
                Q_EMIT(userStatus(current_request,false));
            }
            break;

        case LOGIN:
            check_str = "\"result\":\"ok\"";
            if(str.indexOf(check_str) > -1)
            {
                isUserLogin = true;
                setting.saveRegisterData(current_user.email,current_user.password);
                Q_EMIT(userStatus(current_request,true));
            }
            else
            {
                isUserLogin = false;
                Q_EMIT(userStatus(current_request,false));
            }
            break;

        case AUTO_LOGIN:
            check_str = "\"result\":\"ok\"";
            if(str.indexOf(check_str) > -1)
            {
                isUserLogin = true;
                Q_EMIT(userStatus(current_request,true));
            }
            else
            {
                isUserLogin = false;
                Q_EMIT(userStatus(current_request,false));
            }
            break;

        case REGISTER:
            check_str = "\"result\":\"ok\"";
            if(str.indexOf(check_str) > -1)
            {
                isUserRegister = true;
                Q_EMIT(userStatus(current_request,true));
            }
            else
            {
                isUserRegister = true;
                Q_EMIT(userStatus(current_request,false));
            }
            break;

        case UPLOAD:
            check_str = "\"result\":\"ok\"";
            if(str.indexOf(check_str) > -1)
            {
                Q_EMIT(userStatus(current_request,true));
                str.replace("\\","");
                QJsonDocument d = QJsonDocument::fromJson(str.toUtf8());
                QJsonObject jobj = d.object();

                QJsonValue value = jobj.value(QString("url"));
                clip->setText(value.toString());
            }
            else
            {
                Q_EMIT(userStatus(current_request,false));
            }
            break;
        }
    }
}

void httpManager::uploadProgress(qint64 v, qint64 t)
{
    if( v > 0 && t > 0)
    Q_EMIT(setUploadProgress((int)((v/t)*100)));
}

void httpManager::uploadDone()
{
    Q_EMIT(endUploadProgress());
}
