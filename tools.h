#ifndef TOOLS_H
#define TOOLS_H

#include <QWidget>
#include "defines.h"
#include <QColorDialog>
#include <QMenu>

namespace Ui {
class tools;
}

class tools : public QWidget
{
    Q_OBJECT

public:
    explicit tools(QWidget *parent = 0);
    ~tools();

signals:
    void move_screen(bool);
    void shape(int);
    void colorChanged(QColor);
    void penWidthChanged(int);
    void selectMode(bool);
    void shotscreen();
    void undo();
    void redo();
    void textBGstateChanged(int);

    void registerRequest();
    void loginRequest();
    void uploadRequest();

    void showDropMenu(QRect);
    void copyToClipboard();

public slots:
    void set_upload_progress(int);
    void set_upload_progress_show();
    void set_upload_progress_hide();

    void on_pushButton_circle_clicked();
    void on_pushButton_pen_clicked();
    void on_pushButton_line_clicked();
    void on_pushButton_rect_clicked();
    void on_pushButton_arrow_clicked();
    void on_pushButton_text_clicked();
    void on_pushButton_redo_clicked();
    void on_pushButton_undo_clicked();

    void on_pushButton_rect_with_num_clicked();

    void on_pushButton_move_screen_clicked();

    void on_pushButton_direct_publish_clicked();
    void on_pushButton_drop_menu_clicked();

    void setLabelColor(int);

protected:
 //   void mousePressEvent(QMouseEvent *);
 //   void mouseReleaseEvent(QMouseEvent *);
 //   void mouseMoveEvent(QMouseEvent *);

private:
    QMenu *menu;
    Ui::tools *ui;
};

#endif // TOOLS_H
