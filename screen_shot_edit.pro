#-------------------------------------------------
#
# Project created by QtCreator 2017-03-31T21:32:03
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = screen_shot_edit
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        widget.cpp \
    tools.cpp \
    textcell.cpp \
    httpmanager.cpp \
    dialoglogin.cpp \
    dropmenu.cpp \
    datasettings.cpp \
    top_screen.cpp

HEADERS  += widget.h \
    tools.h \
    defines.h \
    textcell.h \
    httpmanager.h \
    dialoglogin.h \
    dropmenu.h \
    datasettings.h \
    top_screen.h

FORMS    += widget.ui \
    tools.ui \
    textcell.ui \
    dialoglogin.ui \
    dropmenu.ui \
    top_screen.ui

RESOURCES += \
    icons.qrc
