#ifndef USER_DATA_H
#define USER_DATA_H

#include <QDialog>

namespace Ui {
class user_data;
}

class user_data : public QDialog
{
    Q_OBJECT

public:
    explicit user_data(QWidget *parent = 0);
    ~user_data();
    QString email;
    QString passwrod;
    QString phone;
public slots:
    void on_pushButton_ok_clicked();

private:
    Ui::user_data *ui;
};

#endif // USER_DATA_H
