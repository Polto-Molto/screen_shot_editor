#ifndef DROPMENU_H
#define DROPMENU_H

#include <QWidget>

namespace Ui {
class dropMenu;
}

class dropMenu : public QWidget
{
    Q_OBJECT

public:
    explicit dropMenu(QWidget *parent = 0);
    ~dropMenu();

signals:
    void publish();
    void copy();
    void save();

public slots:
    void showDropMenu(QRect);

private:
    Ui::dropMenu *ui;
};

#endif // DROPMENU_H
