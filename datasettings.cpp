#include "datasettings.h"

datasettings::datasettings(QObject *parent) : QObject(parent)
{
    setting = 0;
}

void datasettings::setSettingFile(QString file)
{
    setting = new QSettings(file,QSettings::IniFormat);
}

void datasettings::saveRegisterData(QString user,QString pass)
{
    setting->beginGroup("main_data");
    setting->setValue("user",user);
    setting->setValue("pass",pass);
    setting->endGroup();
}

QString datasettings::getUserName()
{
    QString user;
    setting->beginGroup("main_data");
    user = setting->value("user").toString();
    setting->endGroup();
    return user;
}

QString datasettings::getPassword()
{
    QString pass;
    setting->beginGroup("main_data");
    pass = setting->value("pass").toString();
    setting->endGroup();
    return pass;
}
