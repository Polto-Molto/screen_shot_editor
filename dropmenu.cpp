#include "dropmenu.h"
#include "ui_dropmenu.h"

dropMenu::dropMenu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dropMenu)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Popup);
    connect(ui->toolButton_publish,SIGNAL(clicked()),this,SIGNAL(publish()));
    connect(ui->toolButton_save,SIGNAL(clicked()),this,SIGNAL(save()));
    connect(ui->toolButton_copy,SIGNAL(clicked()),this,SIGNAL(copy()));
    connect(ui->toolButton_hide,SIGNAL(clicked()),this,SLOT(hide()));
}

dropMenu::~dropMenu()
{
    delete ui;
}

void dropMenu::showDropMenu(QRect rect)
{
    this->show();
    this->setGeometry(rect.x()+rect.width()-60,rect.y()+60,100,400);
}
