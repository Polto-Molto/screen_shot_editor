#ifndef TOP_SCREEN_H
#define TOP_SCREEN_H

#include <QWidget>

namespace Ui {
class top_screen;
}

class top_screen : public QWidget
{
    Q_OBJECT

public:
    explicit top_screen(QWidget *parent = 0);
    ~top_screen();

public slots:
    void reset_frames_size();
    void setWindowFullScreenSize();
    QRect getCenterFrameGeometry();

protected :
    bool eventFilter(QObject *object, QEvent *event);

private:
    QRect press_top_left_pos;
    QRect press_top_pos;
    QRect press_top_right_pos;

    QRect press_left_pos;
    QRect press_center_pos;
    QRect press_right_pos;

    QRect press_buttom_left_pos;
    QRect press_buttom_pos;
    QRect press_buttom_right_pos;

    QPoint press_pos;
    QPoint current_pos;

private:
    Ui::top_screen *ui;
};

#endif // TOP_SCREEN_H
