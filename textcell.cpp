#include "textcell.h"
#include "ui_textcell.h"

TextCell::TextCell(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextCell)
{
    ui->setupUi(this);
    this->setModal(true);
    setStyleSheet("background-color:rgba(255,255,255,0);");

    this->setAttribute(Qt::WA_TranslucentBackground);
     ui->lineEdit->setStyleSheet("background-color:rgba(255,255,255,0);");

    ui->lineEdit->setAttribute(Qt::WA_TranslucentBackground);
    this->setWindowFlags(Qt::Popup);
    connect(ui->lineEdit,SIGNAL(returnPressed()),this,SLOT(accept()));
    ui->lineEdit->setFocus();
    ui->lineEdit->setCursorPosition(0);
}

TextCell::~TextCell()
{
    delete ui;
}

QString TextCell::currentText()
{
    return ui->lineEdit->text();
}
