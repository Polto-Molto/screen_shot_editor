#include "dialoglogin.h"
#include "ui_dialoglogin.h"

DialogLogin::DialogLogin(httpManager *hm,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogLogin),
    http_manager(hm)
{
    ui->setupUi(this);
    Qt::WindowFlags flags = windowFlags();

    setWindowFlags(flags  | Qt::X11BypassWindowManagerHint |Qt::FramelessWindowHint| Qt::WindowStaysOnTopHint);

    //setWindowFlags(Qt::Popup);
    user_status = false;
    ui->label_email_entered->hide();

    ui->label_password->hide();
    ui->lineEdit_password->hide();

    ui->label_confirm_password->hide();
    ui->lineEdit_phone->hide();

    ui->pushButton_another_account->hide();
    ui->pushButton_login->hide();
    ui->pushButton_register->hide();
    ui->pushButton_back->hide();
}

DialogLogin::~DialogLogin()
{
    delete ui;
}

void DialogLogin::on_pushButton_close_clicked()
{
    this->hide();
}

void DialogLogin::reload()
{
    user_status = false;
    ui->label_status->setText("Enter Login Email");

    ui->pushButton_another_account->hide();

    ui->lineEdit_email->show();
    ui->pushButton_next->show();
    ui->label_email_entered->hide();

    ui->label_password->hide();
    ui->lineEdit_password->hide();

    ui->label_confirm_password->hide();
    ui->lineEdit_phone->hide();

    ui->pushButton_login->hide();
    ui->pushButton_back->hide();
    ui->pushButton_register->hide();
    show();
}

void DialogLogin::on_pushButton_next_clicked()
{
    http_manager->checkUserFound(ui->lineEdit_email->text());
}

void DialogLogin::on_pushButton_back_clicked()
{
    reload();
}

void DialogLogin::on_pushButton_another_account_clicked()
{
    reload();

}

void DialogLogin::on_pushButton_login_clicked()
{
    http_manager->login(ui->lineEdit_email->text(),ui->lineEdit_password->text());
}

void DialogLogin::on_pushButton_register_clicked()
{
    http_manager->registeration(ui->lineEdit_email->text(),ui->lineEdit_password->text(),ui->lineEdit_phone->text());
}

void DialogLogin::setUserStatus(REQUEST req,bool state)
{
    switch (req) {
    case CHECK_USER:
        if(state)
        {
            ui->label_status->setText("Enter Login Password");

            ui->label_email_entered->setText(ui->lineEdit_email->text());
            ui->lineEdit_email->hide();
            ui->label_email_entered->setText(http_manager->currentUser().email);
            ui->label_email_entered->show();

            ui->pushButton_next->hide();
            ui->label_password->show();
            ui->lineEdit_password->show();
            ui->pushButton_login->show();
            ui->pushButton_back->show();
        }
        else
        {
            ui->label_status->setText("Enter Registartion Data");

            ui->label_email_entered->setText(ui->lineEdit_email->text());
            ui->lineEdit_email->hide();
            ui->label_email_entered->show();

            ui->label_password->show();
            ui->lineEdit_password->show();

            ui->pushButton_next->hide();
            ui->label_confirm_password->show();
            ui->lineEdit_phone->show();
            ui->pushButton_login->hide();
            ui->pushButton_register->show();
            ui->pushButton_back->show();
            Q_EMIT(loginError());
        }
        break;

    case LOGIN:
        if(state)
        {
            ui->label_status->setText("Welcome");
            ui->pushButton_another_account->show();
            ui->label_email_entered->setText(http_manager->currentUser().email);
            ui->label_email_entered->show();

            ui->pushButton_back->hide();
            ui->label_email->hide();
            ui->lineEdit_email->hide();

            ui->label_password->hide();
            ui->lineEdit_password->hide();

            ui->label_confirm_password->hide();
            ui->lineEdit_phone->hide();

            ui->pushButton_login->hide();
            ui->pushButton_register->hide();
            ui->pushButton_next->hide();
            hide();
            Q_EMIT(loginDone());

        }
        else
        {
            ui->label_status->setText("Login Password Error");
            Q_EMIT(loginError());
        }
        break;

    case AUTO_LOGIN:
        if(state)
        {
            ui->label_status->setText("Welcome");
            ui->pushButton_another_account->show();
            ui->label_email_entered->setText(http_manager->currentUser().email);

            ui->label_email_entered->show();

            ui->label_email->hide();
            ui->lineEdit_email->hide();

            ui->label_password->hide();
            ui->lineEdit_password->hide();

            ui->label_confirm_password->hide();
            ui->lineEdit_phone->hide();

            ui->pushButton_login->hide();
            ui->pushButton_register->hide();
            ui->pushButton_next->hide();
            hide();
            Q_EMIT(loginDone());

        }
        else
        {
            ui->label_status->setText("Login Password Error");
            Q_EMIT(loginError());
        }
        break;

    case REGISTER:
        if(state)
        {
            ui->label_status->setText("Welcome");
            ui->label_email_entered->setText(http_manager->currentUser().email);
            ui->label_email_entered->show();

            ui->label_email->hide();
            ui->lineEdit_email->hide();

            ui->label_password->hide();
            ui->lineEdit_password->hide();

            ui->label_confirm_password->hide();
            ui->lineEdit_phone->hide();

            ui->pushButton_login->hide();
            ui->pushButton_register->hide();
            ui->pushButton_next->hide();
            ui->pushButton_back->hide();
            hide();
            Q_EMIT(loginDone());
        }
        else
        {
            ui->label_status->setText("Registration Error");
            Q_EMIT(loginError());
        }
        break;

    case UPLOAD:

        break;

    case SERVER:
//        ui->label_status->setText("Server Response Error");
        break;
    }
}
