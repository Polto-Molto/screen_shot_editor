#ifndef TEXTCELL_H
#define TEXTCELL_H

#include <QDialog>

namespace Ui {
class TextCell;
}

class TextCell : public QDialog
{
    Q_OBJECT

public:
    explicit TextCell(QWidget *parent = 0);
    ~TextCell();

public slots:
    QString currentText();

private:
    Ui::TextCell *ui;
};

#endif // TEXTCELL_H
