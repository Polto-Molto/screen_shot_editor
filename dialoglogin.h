#ifndef DIALOGLOGIN_H
#define DIALOGLOGIN_H

#include <QDialog>
#include "httpmanager.h"

namespace Ui {
class DialogLogin;
}

class DialogLogin : public QDialog
{
    Q_OBJECT

public:
    explicit DialogLogin(httpManager *, QWidget *parent = 0);
    ~DialogLogin();

signals:
    void loginDone();
    void registerDone();

    void loginError();
    void registerError ();

public slots:
    void reload();

    void on_pushButton_next_clicked();
    void on_pushButton_back_clicked();
    void on_pushButton_login_clicked();
    void on_pushButton_register_clicked();
    void on_pushButton_another_account_clicked();
    void on_pushButton_close_clicked();

    void setUserStatus(REQUEST,bool);

private:
    Ui::DialogLogin *ui;
    httpManager *http_manager;
    bool user_status;
};

#endif // DIALOGLOGIN_H
